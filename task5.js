let a = "Hello world";

if (typeof a === "string") { // оператор typeof возвращает строку, указывающую тип операнда
	console.log(a);
}

/*

Скрипт выведет "hello world", так как условие typeof a === "string" истинно (в переменной a записано значение типа String)

*/