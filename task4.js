let a = 0; // инциализация переменной a со значением 0 типа Number
let b; // объявление переменной b

if (a) { // если a == true...
	console.log(a); // вывести в консоль значение a
} else if (b) { // иначе, если b == true...
	console.log(b); // вывести в консоль значение b (undefined)
} else { // иначе...
	console.log('hello world'); // ...вывести в консоль строку "hello world"
}

/*

Скрипт выведет в консоль строку "hello world", т.к. a == 0, что приравнивается к false, b == undefined, что также приравниваниется к false.

*/