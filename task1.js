let myVar; // объявление переменной myVar
myVar = 5; // присвоение переменной myVar значения 5 типа Number
let myNum; // объявление переменной myNum
myNum = myVar; // присвоение переменной myNum значения myVar (5)
myVar = 10; // присвоение переменной myVar значения 10 типа Number
myNum = myVar; // присвоение переменной myNum значения myVar (10)

/*

Значения на выходе:
- myNum == 10
- myVar == 10

*/